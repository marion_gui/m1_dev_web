var app=angular.module('tooLate',[]).controller('tooLateCo', ['$scope','$window',
  function($scope, $window) {
    $scope.event;
    $scope.sousCategorie;
    $scope.today = new Date();
    $scope.user;
    $scope.users;
    
    $scope.troptard =function($event)
    {
      if($event.endDate < $scope.date)
        return true;
      else
        return false;
    }
    
    $scope.todate =function($string)
    {
      return new Date($string);
    }

        // little hack to be sure that apis.google.com/js/client.js is loaded
    // before calling
    // onload -> init() -> window.init() -> then here
    $window.init = function() {
      console.log("windowinit called");
      var rootApi = 'https://ctroptard44.appspot.com/_ah/api/';
      gapi.client.load('evenementendpoint', 'v1', function() {
        console.log("eventloaded");
        gapi.client.evenementendpoint.listEvenement().execute(
          function(resp) {
            $scope.event=resp.items;
            $scope.$apply();
            console.log(resp);
          });
      }, rootApi);
      gapi.client.load('souscategorieendpoint', 'v1', function() {
        console.log("souscategorieloaded");
        gapi.client.souscategorieendpoint.listSousCategorie().execute(
          function(resp) {
            $scope.sousCategorie=resp.items;
            $scope.$apply();
            console.log(resp);
          });
      }, rootApi);
   }
    
    
  $scope.cocherTout = function() {
   var etat = $scope.cocheTout;
   angular.forEach($scope.sousCategorie, function(value){ 
     value.selected = etat; // ajout du selected sur la catégorie

     })
  }

  $scope.validerPreferences = function(){
    $scope.selectedCategories = [];
  //Liste des catégories sélectionnées par l'utilisateur
    angular.forEach($scope.sousCategorie, function(value) {
      if (value.selected === true){
        $scope.selectedCategories.push(value.idsouscat);
      }
  })
  }
  
  signinCallback = function (authResult) {
	  if (authResult['access_token']) {
		  gapi.client.load('oauth2', 'v2', function() {
			  gapi.client.oauth2.userinfo.get().execute(function(resp) {
			    // Shows user email
			    console.log(resp.email);
			    $scope.user = resp.email;
	            $scope.$apply();
			    console.log($scope.user);
			    var rootApi = 'https://ctroptard44.appspot.com/_ah/api/';
			      var user = {"mail" : $scope.user, "score" : 0};
			      gapi.client.load('userendpoint','v1', function(){
			    	  console.log("user saved");
			    	  gapi.client.userendpoint.insertUser(user).execute();
			      }, rootApi);
			      gapi.client.load('userendpoint', 'v1', function() {
			    	  console.log("user list");
			          gapi.client.userendpoint.listUser().execute(
			            function(resp) {
			              $scope.users=resp.items;
			              $scope.$apply();
			            });
			        }, rootApi);
			  });
		  });
			      
		 gapi.auth.setToken(authResult);
	   document.getElementById('afficheNom').setAttribute('style', 'display: inline');
	    document.getElementById('preferences').setAttribute('style', 'display: inline');
	    document.getElementById('users').setAttribute('style', 'display: inline');
	    document.getElementById('signinButton').setAttribute('style', 'display: none');
		document.getElementById('revokeButton').setAttribute('style', 'display: inline');
	  } else if (authResult['error']) {

	    console.log('Une erreur s\'est produite : ' + authResult['error']);
	  }
	}
	  
	disconnectUser = function (access_token) {
	  var revokeUrl = 'https://accounts.google.com/o/oauth2/revoke?token=' +
	      access_token;

	  // Exécuter une requête GET asynchrone.
	  $.ajax({
	    type: 'GET',
	    url: revokeUrl,
	    async: false,
	    contentType: "application/json",
	    dataType: 'jsonp',
	    success: function(nullResponse) {
		 document.getElementById('afficheNom').setAttribute('style', 'display: none');
		 document.getElementById('users').setAttribute('style', 'display: none');
	     document.getElementById('preferences').setAttribute('style', 'display: none');
	    document.getElementById('signinButton').setAttribute('style', 'display: inline');
		document.getElementById('revokeButton').setAttribute('style', 'display: none');
	    },
	    error: function(e) {
	      // Gérer l'erreur
	      // console.log(e);
	      // Orienter éventuellement les utilisateurs vers une dissociation manuelle en cas d'échec
	      // https://plus.google.com/apps
	    }
	  });
	}    
  }
]);


(function() {

    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;

    po.src = 'https://apis.google.com/js/client:plusone.js';

    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);

  })();