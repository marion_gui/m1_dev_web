package projet;
import java.util.ArrayList;
import java.util.List;

import javax.jdo.annotations.*;

import com.google.appengine.datanucleus.annotations.Unowned;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Categorie {
	
	@PrimaryKey
	private String idcat;
	
	@Persistent
	private String namecat;
	
	public Categorie(){}
	
	public Categorie(String id, String name) {
		this.namecat = name;
		this.idcat = id;
	}

	public String getId() {
		return idcat;
	}


	public void setId(String key) {
		this.idcat = key;
	}


	public String getName() {
		return namecat;
	}


	public void setName(String name) {
		this.namecat = name;
	}

	public String getIdcat() {
		return idcat;
	}

	public void setIdcat(String idcat) {
		this.idcat = idcat;
	}

	public String getNamecat() {
		return namecat;
	}

	public void setNamecat(String namecat) {
		this.namecat = namecat;
	}
}
