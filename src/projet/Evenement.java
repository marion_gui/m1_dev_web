package projet;

import java.util.ArrayList;
import java.util.Arrays;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(identityType = IdentityType.APPLICATION, detachable = "true")
public class Evenement {
	
	@PrimaryKey
    private String idevent;
	
	@Persistent
	private String nameevent;
	
	@Persistent
	private String cityevent;
	
	@Persistent
	private String startdate;
	
	@Persistent
	private String enddate;
	
	@Persistent
	private String geoLongitude;
	
	@Persistent
	private String geoLatitude;
	
    @Persistent
    private ArrayList<String> categorie;

	public Evenement(String idevent, String nameevent, String cityevent,
			String startdate, String enddate, String geoLongitude,
			String geoLatitude, ArrayList<String> categorie) {
		super();
		this.idevent = idevent;
		this.nameevent = nameevent;
		this.cityevent = cityevent;
		this.startdate = startdate;
		this.enddate = enddate;
		this.geoLongitude = geoLongitude;
		this.geoLatitude = geoLatitude;
		this.categorie = categorie;
	}

	public String getIdevent() {
		return idevent;
	}

	public void setIdevent(String idevent) {
		this.idevent = idevent;
	}

	public String getNameevent() {
		return nameevent;
	}

	public void setNameevent(String nameevent) {
		this.nameevent = nameevent;
	}

	public String getCityevent() {
		return cityevent;
	}

	public void setCityevent(String cityevent) {
		this.cityevent = cityevent;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String getEnddate() {
		return enddate;
	}

	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}

	public String getGeoLongitude() {
		return geoLongitude;
	}

	public void setGeoLongitude(String geoLongitude) {
		this.geoLongitude = geoLongitude;
	}

	public String getGeoLatitude() {
		return geoLatitude;
	}

	public void setGeoLatitude(String geoLatitude) {
		this.geoLatitude = geoLatitude;
	}

	public ArrayList<String> getCategorie() {
		return categorie;
	}

	public void setCategorie(ArrayList<String> categorie) {
		this.categorie = categorie;
	}

	@Override
	public String toString() {
		return "Evenement [idevent=" + idevent + ", nameevent=" + nameevent
				+ ", cityevent=" + cityevent + ", startdate=" + startdate
				+ ", enddate=" + enddate + ", geoLongitude=" + geoLongitude
				+ ", geoLatitude=" + geoLatitude + ", categorie="
				+ categorie.toString() + "]";
	}
	
	
}
