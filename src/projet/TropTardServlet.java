package projet;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

import javax.jdo.*;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

@SuppressWarnings("serial")
public class TropTardServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		
		resp.setContentType("text/html; charset=ISO-8850-1");
		
		try {
			PersistenceManager pm = PMF.get().getPersistenceManager();
			JsonFactory jfactory = new JsonFactory();
			ArrayList<String> agenda = new ArrayList();
			HashSet<String> categorie_niveau1_id = new HashSet();
			HashSet<String> categorie_id = new HashSet();
			HashSet<String> event_id = new HashSet();
			agenda.add("AgendaLoisirs_rootAgendaCatIds");
			agenda.add("AgendaMobile_rootAgendaCatIds");
			String json;

			//On recupere les ID des catégories de premier niveau
			for(String sagenda : agenda){
				URL url = new URL("http://api.loire-atlantique.fr:80/opendata/1.0/parameter/" + sagenda);
				BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));


				while ((json = reader.readLine()) != null){

					JsonParser jParser = jfactory.createJsonParser(json);

					while (jParser.nextToken() != JsonToken.END_OBJECT){
						String fieldname = jParser.getCurrentName();

						//On stocke les ID des categories de premier niveau
						if ("value".equals(fieldname)) {
							jParser.nextToken();
							String valeur = jParser.getText();
							for (String retval: valeur.split(",")){
								categorie_niveau1_id.add(retval);
								categorie_id.add(retval);
							}

						}
					}


				}
			}

			for(String id : categorie_niveau1_id){
				URL url = new URL("http://api.loire-atlantique.fr:80/opendata/1.0/category/" + id);
				BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
				String id_cat = "";
				String name = "";

				while ((json = reader.readLine()) != null){
					JsonParser jParser = jfactory.createJsonParser(json);

					while (jParser.nextToken() != JsonToken.END_OBJECT){
						String fieldname = jParser.getCurrentName();
						//On stocke les ID des sous categories des categories
						if ("id".equals(fieldname)) {
							jParser.nextToken();
							String valeur = jParser.getText();
							for (String retval: valeur.split(",")){
								id_cat = retval;
							}
						}

						if ("name".equals(fieldname)) {
							jParser.nextToken();
							String valeur = jParser.getText();
							for (String retval: valeur.split(",")){
								name = retval;
							}
						}
					}
				}

				Categorie cat = new Categorie(id_cat, name);

				URL url_children = new URL("http://api.loire-atlantique.fr:80/opendata/1.0/category/" + id_cat + "/children");
				BufferedReader reader_children = new BufferedReader(new InputStreamReader(url_children.openStream()));
				String id_cat_children = "";
				String name_children = "";

				while ((json = reader_children.readLine()) != null){
					//resp.getWriter().println(json);
					JsonParser jParser = jfactory.createJsonParser(json);

					while (jParser.nextToken() != JsonToken.END_OBJECT){
						String fieldname = jParser.getCurrentName();
						//On stocke les ID des sous categories des categories
						if ("children".equals(fieldname)) {
							while (jParser.nextToken() != JsonToken.END_ARRAY) {
								while (jParser.nextToken() != JsonToken.END_OBJECT){
									String fieldname2 = jParser.getCurrentName();
									if ("id".equals(fieldname2)) {
										jParser.nextToken();
										String valeur = jParser.getText();
										for (String retval: valeur.split(",")){
											id_cat_children = retval;
										}
									}
									if ("name".equals(fieldname2)) {
										jParser.nextToken();
										String valeur = jParser.getText();
										for (String retval: valeur.split(",")){
											name_children = retval;
										}
									}
								}

								//stockage des sous categories
								SousCategorie souscat = new SousCategorie(id_cat_children, name_children, id);
								pm.currentTransaction().begin();
								try {
									pm.makePersistent(souscat);
									pm.currentTransaction().commit();
								} finally {
									if (pm.currentTransaction().isActive()) {
										pm.currentTransaction().rollback();
									}
								}

								//On recupere les evenements des categories
								URL url_event = new URL("http://api.loire-atlantique.fr:80/opendata/1.0/event/summary?catIds=" + id_cat_children+ "&periodOfTime=30&itemsPerPage=100");
								BufferedReader reader_event = new BufferedReader(new InputStreamReader(url_event.openStream()));
								String idevent = "";
								int nbItem = 0;

								while ((json = reader_event.readLine()) != null){
									//resp.getWriter().println(json);
									JsonParser jParserEvent = jfactory.createJsonParser(json);


									while (jParserEvent.nextToken() != JsonToken.END_OBJECT){
										String fieldnameEvent = jParserEvent.getCurrentName();
										if ("nbItems".equals(fieldnameEvent)) {
											jParserEvent.nextToken();
											String valeur = jParserEvent.getText();
											for (String retval: valeur.split(",")){
												nbItem = Integer.parseInt(retval);
											}
										}

									}
									//resp.getWriter().println(nbItem);
									int iterateur = 0;

									while (iterateur < nbItem){
										jParserEvent.nextToken();
										String fieldnameEvent = jParserEvent.getCurrentName();

										if ("eventId".equals(fieldnameEvent)) {
											jParserEvent.nextToken();
											String valeur = jParserEvent.getText();
											for (String retval: valeur.split(",")){
												idevent = retval;
											}
										}

										if ("geoPrecision".equals(fieldnameEvent)) {
											//resp.getWriter().println("idevent : " +idevent);
											event_id.add(idevent);
											iterateur++;
											jParserEvent.nextToken();
										}
									}
								}

							}
						}
					}
				}
				//stockage des catégories
				pm.currentTransaction().begin();
				try {
					pm.makePersistent(cat);
					pm.currentTransaction().commit();
				} finally {
					if (pm.currentTransaction().isActive()) {
						pm.currentTransaction().rollback();
					}
				}
			}


			for(String id : event_id){
				URL url = new URL("http://api.loire-atlantique.fr:80/opendata/1.0/event/" + id);
				BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
				String nameevent = "";
				String cityevent = "";
				String startdate = "";
				String enddate = "";
				String geoLongitude = "";
				String geoLatitude = "";
				ArrayList<String> categorie = new ArrayList<String>();

				while ((json = reader.readLine()) != null){
					//resp.getWriter().println(json);
					JsonParser jParserEvent = jfactory.createJsonParser(json);
					boolean stop = false;
					boolean titleOk = false;
					boolean categorieOk = false;
					int nbCategorie = 0;

					while (!stop){
						jParserEvent.nextToken();
						String fieldnameEvent = jParserEvent.getCurrentName();

						if ("title".equals(fieldnameEvent)) {
							jParserEvent.nextToken();
							String valeur = jParserEvent.getText();
							for (String retval: valeur.split(",")){
								if(!titleOk)
								{
									nameevent = retval;
									titleOk = true;
								}
								else
								{
									cityevent = retval;
								}
							}
						}
						if ("category".equals(fieldnameEvent)) {
							jParserEvent.nextToken();
							categorieOk = true;
						}
						if ("id".equals(fieldnameEvent) && categorieOk) {
							jParserEvent.nextToken();
							String valeur = jParserEvent.getText();
							for (String retval: valeur.split(",")){
								categorie.add(retval);
								nbCategorie++;
							}
						}

						if ("startDate".equals(fieldnameEvent)) {
							//jParserEvent.nextToken();
							String valeur = jParserEvent.getText();
							for (String retval: valeur.split(",")){
								startdate = retval;
							}
						}
						if ("geoLongitude".equals(fieldnameEvent)) {
							//jParserEvent.nextToken();
							String valeur = jParserEvent.getText();
							for (String retval: valeur.split(",")){
								geoLongitude = retval;
							}
						}
						if ("geoLatitude".equals(fieldnameEvent)) {
							//jParserEvent.nextToken();
							String valeur = jParserEvent.getText();
							for (String retval: valeur.split(",")){
								geoLatitude = retval;
							}
						}
						if ("endDate".equals(fieldnameEvent)) {
							jParserEvent.nextToken();
							String valeur = jParserEvent.getText();
							for (String retval: valeur.split(",")){
								enddate = retval;
							}
						}
						if ("geoPrecision".equals(fieldnameEvent)) {
							stop = true;
							jParserEvent.nextToken();
						}
					}
					Evenement ev = new Evenement(id, nameevent, cityevent,
							startdate, enddate, geoLongitude, geoLatitude, categorie);
					resp.getWriter().println(ev.toString());

					pm.currentTransaction().begin();
					try {
						pm.makePersistent(ev);
						pm.currentTransaction().commit();
					} finally {
						if (pm.currentTransaction().isActive()) {
							pm.currentTransaction().rollback();
						}
					}
				}
			}



		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}	

