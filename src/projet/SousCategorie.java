package projet;

import javax.jdo.annotations.*;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.datanucleus.annotations.Unowned;

@PersistenceCapable(identityType = IdentityType.APPLICATION, detachable = "true")
public class SousCategorie {
	
	@PrimaryKey
    private String idsouscat;
	
	@Persistent
	private String namesouscat;
	
    @Persistent
    private String categoriesouscat;
	
	public SousCategorie(){}

	public SousCategorie(String id, String name, String categorie) {
		super();
		this.idsouscat = id;
		this.namesouscat = name;
		this.categoriesouscat = categorie;
	}

	public String getIdsouscat() {
		return idsouscat;
	}

	public void setIdsouscat(String idsouscat) {
		this.idsouscat = idsouscat;
	}

	public String getNamesouscat() {
		return namesouscat;
	}

	public void setNamesouscat(String namesouscat) {
		this.namesouscat = namesouscat;
	}

	public String getCategoriesouscat() {
		return categoriesouscat;
	}

	public void setCategoriesouscat(String categoriesouscat) {
		this.categoriesouscat = categoriesouscat;
	}
}
