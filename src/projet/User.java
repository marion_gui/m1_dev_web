package projet;

import java.util.ArrayList;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class User {
	
	@PrimaryKey
	@Persistent
	private String mail;
	@Persistent
	private Integer score;
	@Persistent
	private ArrayList<String> categories;
	
	
	public User(){}


	public User(String idUser, String mail, ArrayList<String> categories, Integer score) {
		super();
		this.mail = mail;
		this.score = score;
		this.categories = categories;
	}


	
	
	public Integer getScore() {
		return score;
	}


	public void setScore(Integer score) {
		this.score = score;
	}


	public String getMail() {
		return mail;
	}


	public void setMail(String mail) {
		this.mail = mail;
	}


	public ArrayList<String> getCategories() {
		return categories;
	}


	public void setCategories(ArrayList<String> categories) {
		this.categories = categories;
	}
	
	
}
